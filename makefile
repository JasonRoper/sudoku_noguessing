PROJECT=sudoku

LIBS=catch2
INCLUDE=$(patsubst %,lib/%/include, $(LIBS))
LIB_HEADERS=$(foreach dir, $(INLCUDE), $(wildcard $(dir)/*))

HEADERS=$(wildcard src/*.hpp) $(LIB_HEADERS)

BUILD_DIR=build

FILES=$(wildcard src/*.cpp)
OBJS=$(patsubst src/%.cpp,$(BUILD_DIR)/%.o, $(FILES))

CPP=g++
OPTS=-std=c++2a -Wall -Wextra -g $(foreach inc, $(INCLUDE), -I$(inc))

.PHONY: all test clean

all : $(PROJECT)

test : $(PROJECT)-test

clean :
	rm -rf build
	rm -f $(PROJECT) $(PROJECT)-test

$(PROJECT)-test : build/catch2/catch-main.o $(filter-out build/main.o build/dummy.o,$(OBJS))
	$(CPP) $(OPTS) -o $@ $^

$(PROJECT) : $(OBJS)
	$(CPP) $(OPTS) -o $@ $^

$(OBJS): $(BUILD_DIR)/%.o : src/%.cpp $(HEADERS)
	@mkdir -p $(BUILD_DIR)
	$(CPP) $(OPTS) -c -o $@ $<

$(BUILD_DIR)/catch2/%.o : lib/catch2/%.cpp lib/catch2/include/catch.hpp
	@mkdir -p $(@D)
	$(CPP) $(OPTS) -c -o $@ $<