#pragma once
#include <vector>
#include <istream>
#include <optional>
#include <bitset>

typedef std::optional<int> Cell;

class Board {
    std::vector<Cell> board;
public:
    Board(std::istream& input);

    const Cell& get(std::size_t x, std::size_t y) const;
    void set(std::size_t x, std::size_t y, int value);

    bool is_solved() const;
};

struct Solver {
    Board board;
    std::vector<std::bitset<9>> rows;
    std::vector<std::bitset<9>> cols;
    std::vector<std::bitset<9>> squares;
public:
    Solver(Board board);
    void set(std::size_t x, std::size_t y, int val);
    void calculate(std::size_t x, std::size_t y);
    void recalculate(std::size_t x, std::size_t y);
};

Board solve(const Board& board);

std::size_t square_index(std::size_t x, std::size_t y);
std::size_t x_from_square(std::size_t square, std::size_t subsquare);
std::size_t y_from_square(std::size_t square, std::size_t subsquare);

std::istream& operator>> (std::istream& in, Cell& value);

std::ostream& operator<< (std::ostream& out, const Board& board);