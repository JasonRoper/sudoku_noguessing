// this file exists to stop catch (testing framework) from complaining when
// building the main file (not the test). - this should probably have been
// done by always wrapping the testing stuff in #ifdef TESTING or something
// similar, but this is the easier solution.

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
