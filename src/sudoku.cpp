#include "sudoku.hpp"
#include <vector>
#include <bitset>
#include <optional>
#include <exception>
#include <istream>
#include <iostream>

std::istream& operator>> (std::istream& in, Cell& value) {
    int val;
    in >> val;
    if (val < 1 || val > 9) {
        value = std::nullopt;
    } else {
        value = val;
    }

    return in;
}

Board::Board(std::istream& input): board(9*9) {
    for (auto& cell: board) {
        input >> cell;
    }
}

const Cell& Board::get(std::size_t x, std::size_t y) const {
    return board[y * 9 + x];
}

void Board::set(std::size_t x, std::size_t y, int value) {
    if (value > 9 || value < 1) {
        throw std::invalid_argument("suduku.cpp::Board::set: value is outside of the range [1,9]");
    }
    board[y * 9 + x] = value;
}

bool Board::is_solved() const {

    for (std::size_t i =  0 ; i < 9 ; i++) {
        std::bitset<9> row_set;
        std::bitset<9> col_set;
        std::bitset<9> square_set;

        for (std::size_t j = 0 ; j < 9 ; j++) {

            auto row = get(j, i);
            if (row && !row_set[*row - 1]) {
                row_set.set(*row - 1);
            } else {
                return false;
            }

            auto col = get(j, i);

            if (col && !col_set[*col - 1]) {
                col_set.set(*col - 1);
            } else {
                return false;
            }

            std::size_t square_x = x_from_square(i, j);
            std::size_t square_y = y_from_square(i, j);
            auto square = get(square_x, square_y);

            if (square && !square_set[*square - 1]) {
                square_set.set(*square - 1);
            } else {
                return false;
            }
        }
    }
    return true;
}

std::size_t square_index(std::size_t x, std::size_t y) {
    return (y / 3) * 3 + x / 3;
}

std::size_t x_from_square(std::size_t square, std::size_t subsquare) {
    return (square % 3) * 3 + subsquare % 3;
}

std::size_t y_from_square(std::size_t square, std::size_t subsquare) {
    return (square / 3) * 3 + subsquare / 3;
}

Solver::Solver(Board board):
        board(board),
        rows(9, std::bitset<9>().set()),
        cols(9, std::bitset<9>().set()),
        squares(9, std::bitset<9>().set())
{
    // go through everything in the board and remove the values that
    // have already been set from the bitset (rows/cols/squares are
    // initialized to all 1s)
    for (std::size_t y = 0 ; y < 9 ; y++) {
        for (std::size_t x = 0 ; x < 9 ; x++) {
            if (auto val = board.get(x,y)) {
                rows[y].set(*val - 1, false);
                cols[x].set(*val - 1, false);
                squares[square_index(x,y)].set(*val - 1, false);
            }
        }
    }
}

void Solver::set(std::size_t x, std::size_t y, int val) {
    rows[y].set(val, false);
    cols[x].set(val, false);
    squares[square_index(x,y)].set(val, false);
    // need to add 1 since val is 0 based, not 1
    board.set(x,y, val + 1);
}

void Solver::calculate(std::size_t x, std::size_t y) {
    // don't do anything if the cell already has a assigned value
    if (board.get(x,y)) return;

    // check the values that cell x,y can be, and if there is only one,
    // add it, and then recalculate the row, col, square that cell x,y
    // is in.
    auto possible = rows[y] & cols[x] & squares[square_index(x,y)];
    if (possible.count() == 1) {
        for (std::size_t i = 0 ; i < possible.size() ; i++) {
            if (possible[i]) {
                set(x, y, i);
                recalculate(x,y);
                return;
            }
        }
    }
}

void Solver::recalculate(std::size_t x, std::size_t y) {
    std::size_t square = square_index(x, y);

    // go through every value that could have been affected
    // by cell x,y being assigned a value, and see if there are
    // any deductions that can be made.
    for(std::size_t i = 0 ; i < 9 ; i++) {
        // row
        calculate(x, i);
        // col
        calculate(i, y);
        // square
        std::size_t square_x = x_from_square(square, i);
        std::size_t square_y = y_from_square(square, i);
        calculate(square_x, square_y);
    }
}

Board solve(const Board& board) {
    Solver s(board);

    for (std::size_t y = 0 ; y < 9 ; y++) {
        for (std::size_t x = 0 ; x < 9 ; x++) {
            s.calculate(x, y);
        }
    }
    return s.board;
}

std::ostream& operator<< (std::ostream& out, const Board& board) {
    for (std::size_t y = 0 ; y < 9 ; y++) {
        for (std::size_t x = 0 ; x < 9 ; x++) {
            if (auto value = board.get(x,y)) {
                out << *value << ' ';
            } else {
                out << "0 ";
            }
        }
        out << '\n';
    }
    return out;
}

// ----- TESTING STARTS HERE -----

#include "catch.hpp"
#include <fstream>
#include <tuple>

TEST_CASE("suduku solver works") {
    std::vector<std::string> test_files = {
        "res/test1.in",
        "res/test2.in"
    };
    for (auto& filename : test_files) {
        SECTION(filename) {
            std::ifstream input(filename);

            Board board(input);
            Board res = solve(board);

            REQUIRE(res.is_solved());
        }
    }
}


TEST_CASE("square coordinates work") {
    std::vector<std::tuple<std::string,std::size_t,std::size_t, std::size_t, std::size_t>> tests = {
        {"origin", 0, 0, 0, 0},
        {"first line first square",0, 1, 1, 0},
        {"second line first square", 1, 0, 3, 0},
        {"last square", 8, 0, 6, 6},
        {"first line last square", 8, 2, 8, 6},
        {"second line last square", 8, 4, 7, 7},
        {"3rd line 3rd square", 2, 7, 7, 2}
    };
    for (auto test : tests) {
        auto [name, square, subsquare, x, y] = test;
        SECTION(name) {
            INFO("square: " << square << " subsquare: " << subsquare << " x: " << x << " y: " << y);
            std::size_t square_x = x_from_square(square, subsquare);
            std::size_t square_y = y_from_square(square, subsquare);
            REQUIRE(square_x == x);
            REQUIRE(square_y == y);
        }
    }
}