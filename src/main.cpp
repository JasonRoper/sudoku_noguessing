#include <iostream>
#include <istream>
#include <fstream>
#include <sstream>

#include "sudoku.hpp"

int main(int argc, char** argv) {
    auto test1 = std::istringstream(
            "4 5 0 8 0 0 9 0 0 "
            "0 9 0 0 5 6 0 0 4 "
            "1 0 0 0 0 0 0 0 7 "
            "2 6 0 5 4 0 0 9 0 "
            "0 0 4 1 0 2 3 0 0 "
            "0 7 0 0 6 9 0 4 8 "
            "7 0 0 0 0 0 0 0 9 "
            "8 0 0 4 9 0 0 7 0 "
            "0 0 9 0 0 3 0 2 5 ");

    Board board = Board(test1);
    if (argc > 1) {
        auto file = std::ifstream(argv[1]);
        board = Board(file);
    }

    Board res = solve(board);
    std::cout << "solved: " << ((res.is_solved()) ? "yes": "no") << '\n';
    std::cout << res << std::endl;

    return 0;
}